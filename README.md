# nsd_flutter

NSD Service plugin for Flutter(For now just Andoid, ~~iOS~~)

for more information please visit : [wiki](https://en.wikipedia.org/wiki/NSD) and [android dev site](https://developer.android.com/training/connect-devices-wirelessly)


# Todo
 - [x] Initialize NSD Service and Just checking Registering
 - [ ] Complete Registering for Android
 - [ ] Complete Discovering for Android
 - [ ] Complete Android Example
 - [ ] Complete Registering for iOS
 - [ ] Complete Registering for iOS
